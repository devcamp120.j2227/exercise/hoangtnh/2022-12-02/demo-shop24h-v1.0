import "bootstrap/dist/css/bootstrap.min.css";
import Header from "./components/headerComponent/headerComponent";
import Footer from "./components/footerComponent/footerComponent";
import Body from "./components/bodyComponent/bodyComponent";
function App() {
  return (
    <div >
        <Header/>
        <Body/>
        <Footer/>
    </div>
  );
}

export default App;
