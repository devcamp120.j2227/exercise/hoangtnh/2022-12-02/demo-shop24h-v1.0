import { Component } from "react";
import "./footer.css";
class Footer extends Component{
    render(){
        return(
            <div>
                <div className="d-flex flex-row justify-content-center footer-div">
                    <div className="col-sm-3">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        </p>
                        <p>
                            ©  2018 . All Rights Reserved.
                        </p>
                    </div>
                    <div className="flex-column col-sm-3 text-align-center footer-column">
                        <h3>Contacts</h3>
                        <h4>Address:</h4>
                        <p>Kolkata, West Bengal, India</p>
                        <h4>email:</h4>
                        <p>info@example.com</p>
                        <h4>phones:</h4>
                        <p>+91 99999999 or +91 11111111</p>
                    </div>
                    <div className="flex-column col-sm-3 text-align-center footer-column">
                        <h4>Links</h4>
                        <li className="mb-3"><a className="a-footer" href="/">About</a></li>
                        <li className="mb-3"><a className="a-footer" href="/"> Projects</a></li>
                        <li className="mb-3"><a className="a-footer"href="/">Blog</a></li>
                        <li className="mb-3"><a className="a-footer" href="/">Contacts</a></li>
                        <li className="mb-3"><a className="a-footer" href="/">Pricing</a></li>
                    </div>
                </div>
                <div className="d-flex flex-row footer-div">
                    <div className="col-3 social">FACEBOOK</div>
                    <div className="col-3 social">INSTAGRAM</div>
                    <div className="col-3 social">TWITTER</div>
                    <div className="col-3 social">GOOGLE</div>
                </div>
            </div>
        )
    }
}

export default Footer;